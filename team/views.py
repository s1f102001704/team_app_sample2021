from django.shortcuts import render, redirect
from django.http import Http404, JsonResponse
from django.core.exceptions import SuspiciousOperation
from django.views.decorators.csrf import csrf_exempt
import urllib
import json
import random

from .models import Reply,Twitter

WEBHOOK_URL = 'https://hooks.slack.com/services/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'
VERIFICATION_TOKEN = 'DzzWF1d0dNwX9KXgFIYGFese'
ACTION_HOW_ARE_YOU = 'HOW_ARE_YOU'

def index(request):
    dog_replies = Reply.objects.filter(response=Reply.POSITIVE)
    cat_replies = Reply.objects.filter(response=Reply.NEUTRAL)
    rabbit_replies = Reply.objects.filter(response=Reply.NEGATIVE)

    context = {
        'dog_replies': dog_replies,
        'cat_replies': cat_replies,
        'rabbit_replies': rabbit_replies,

    }
    return render(request, 'index.html', context)

def clear(request):
    Reply.objects.all().delete()
    return redirect(index)

def announce(request):
    if request.method == 'POST':
        data = {
            'text': request.POST['message']
        }
        post_message(WEBHOOK_URL, data)

    return redirect(index)

def twi(request):
    if request.method == "POST":
        try:
            old = Twitter.objects.filter(user_id=request.POST["userid"])
            old.delete()
        except:
            pass

        twitter = Twitter()
        twitter.user_id = request.POST["userid"]
        twitter.Twitter_id = request.POST["twitterid"]
        twitter.save()
        data = {
            "text": "学籍番号:" + twitter.user_id + " Twitter_id@" + twitter.Twitter_id + "を登録"
        }
        post_message(WEBHOOK_URL, data)
    return redirect(index)

@csrf_exempt
def twisearch(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    student_id = request.POST['text']
    user_name = request.POST['user_name']
    user = request.POST['user_id']

    try:
        twid = Twitter.objects.get(user_id=student_id)
        result = {
        'text': '学籍番号:{}のTwitterはhttps://twitter.com/{}?s=20'.format(student_id,twid.Twitter_id),
        'response_type': 'in_channel'
        }
    except:
        result = {
        'text': '<@{}> 学籍番号:{}のTwitterは登録されていません。'.format(user, student_id),
        'response_type': 'in_channel'
        }

    return JsonResponse(result)


@csrf_exempt
def echo(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']

    result = {
        'text': '<@{}> {}'.format(user_id, content.upper()),
        'response_type': 'in_channel'
    }

    return JsonResponse(result)

@csrf_exempt
def hello(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    user_name = request.POST['user_name']
    user_id = request.POST['user_id']
    content = request.POST['text']

    result = {
        'blocks': [
            {
                'type' : 'section',
                'text' : {
                    'type': 'mrkdwn',
                    'text': '<@{}> Which Do You Like?'.format(user_id)
                },
                'accessory': {
                    'type': 'static_select',
                    'placeholder': {
                        'type': 'plain_text',
                        'text': 'I like:',
                        'emoji': True
                    },
                    'options': [
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'Dogs.',
                                'emoji': True
                            },
                            'value': 'positive'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'Cats.',
                                'emoji': True
                            },
                            'value': 'neutral'
                        },
                        {
                            'text': {
                                'type': 'plain_text',
                                'text': 'Rabbits.',
                                'emoji': True
                            },
                            'value': 'negative'
                        }
                    ],
                    'action_id': ACTION_HOW_ARE_YOU
                }
            }
        ],
        'response_type': 'in_channel'
    }

    return JsonResponse(result)

@csrf_exempt
def reply(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    payload = json.loads(request.POST.get('payload'))
    print(payload)
    if payload.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')
    
    if payload['actions'][0]['action_id'] != ACTION_HOW_ARE_YOU:
        raise SuspiciousOperation('Invalid request.')
    
    user = payload['user']
    selected_value = payload['actions'][0]['selected_option']['value']
    response_url = payload['response_url']

    if selected_value == 'positive':
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.POSITIVE)
        reply.save()
        response = {
            'text': '<@{}> Ok, thank you! :dog:'.format(user['id'])
        }
    elif selected_value == 'neutral':
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.NEUTRAL)
        reply.save()
        response = {
            'text': '<@{}> Ok, thank you! :cat:'.format(user['id'])
        }
    else:
        reply = Reply(user_name=user['name'], user_id=user['id'], response=Reply.NEGATIVE)
        reply.save()
        response = {
            'text': '<@{}> Ok, thank you! :rabbit:'.format(user['id'])
        }
    
    post_message(response_url, response)

    return JsonResponse({})

def post_message(url, data):
    headers = {
        'Content-Type': 'application/json',
    }
    req = urllib.request.Request(url, json.dumps(data).encode(), headers)
    with urllib.request.urlopen(req) as res:
        body = res.read()

def delay(request):
    if request.method == 'POST':
        result = request.POST['delaymesse'] + "遅刻します。"
        data = {
            'text': result,
        }
        post_message(WEBHOOK_URL, data)

    return redirect(index)

def shinjirou(request):
    if request.method == 'POST':
        message = request.POST['shinjirou']
        result = message + "ということは、"+ message + "ということです。"
        data = {
            'text': result,
        }
        post_message(WEBHOOK_URL, data)

    return redirect(index) 

def motive(request):
    word = ['じゃあいつやるか、今でしょ！','今日をがんばった者,今日をがんばり始めた者にのみ,明日が来るんだよ',
    'あきらめたらそこで試合終了だよ','今がんばらなければ、いつがんばる','なせばなるなさねばならぬ何事も',
    '諦めんなよ！ 諦めんなよ、お前！！',
    'がんばれがんばれできるできる絶対できるがんばれもっとやれるって！！ やれる気持ちの問題だがんばれがんばれそこだそこだ諦めんな絶対にがんばれ積極的にポジティブにがんばれがんばれ！！',]
    celebrity = ['林修','班長大槻','安西光義','藤田晋','上杉鷹山','松岡修造','松岡修造']
    choice = random.randint(0, len(word) - 1)
    if request.method == 'POST':
        a = {
            'text': word[choice]
        }
        b = {
            'text': celebrity[choice]
        }
        post_message(WEBHOOK_URL, a)
        post_message(WEBHOOK_URL, b)
    
    return redirect(index)
