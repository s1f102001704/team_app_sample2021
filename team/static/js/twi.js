function check() {
    var userid = document.getElementById("userid").value;
    var pattern = /^\w{10}$/g;
    if(userid.match(pattern)){
        return true;    
    }
    window.alert("The student ID is incorrect.");
    return false;
}
